package com.example.app93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
public class ImcActivity extends AppCompatActivity{
    private TextView txtResultado;
    private EditText txtAltura, txtPeso;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imc);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty()){
                    Toast.makeText(getApplicationContext(),"No deje espacios vacíos",Toast.LENGTH_SHORT).show();
                }
                else{
                    float altura = Float.parseFloat(txtAltura.getText().toString());
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    txtResultado.setText(String.valueOf(peso / (altura*altura)));
                }

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty()){
                    Toast.makeText(getApplicationContext(),"No hay nada que limpiar",Toast.LENGTH_SHORT).show();
                }
                else{
                    txtResultado.setText("");
                    txtPeso.setText("");
                    txtAltura.setText("");
                }

            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void iniciarComponentes(){
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
    }

    public boolean valiEmpty(){
        if(txtPeso.getText().toString().isEmpty() ||
        txtAltura.getText().toString().isEmpty() ||
        txtResultado.getText().toString().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
}
