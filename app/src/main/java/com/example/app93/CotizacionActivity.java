package com.example.app93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity{
    private TextView txtUsuario, txtFolio, txtPagoInicial, txtPagoMensual;
    private EditText txtPorcentaje, txtValorAuto, txtDescripcion;
    private RadioButton rdb12, rdb18, rdb24, rdb36;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private RadioGroup grupo;
    private Cotizacion coti = new Cotizacion();
    private int folio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();

        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");

        if (cliente != null) {
            txtUsuario.setText(cliente);
        } else {
            Toast.makeText(CotizacionActivity.this, "Usuario no capturado", Toast.LENGTH_SHORT).show();
        }

        Cotizacion coti = new Cotizacion();
        folio = coti.generarId();

        if(folio > 0){
            txtFolio.setText(String.format("Folio: %d",folio));
        }else{
            Toast.makeText(CotizacionActivity.this, "Folio no capturado", Toast.LENGTH_SHORT).show();
        }


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescripcion.getText().toString().matches("")
                        || txtValorAuto.getText().toString().matches("")
                        || txtPorcentaje.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this, "Falta Ingresar Datos", Toast.LENGTH_SHORT).show();
                    txtDescripcion.requestFocus();
                }else {
                    calcular();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValorAuto.setText("");
                txtPorcentaje.setText("");
                txtPagoInicial.setText("");
                txtPagoMensual.setText("");
                grupo.clearCheck();
            }
        });
    }
    private void calcular() {
        String descripcion = txtDescripcion.getText().toString();
        float valorAuto = Float.parseFloat(txtValorAuto.getText().toString());
        float porcentajePagoInicial = Float.parseFloat(txtPorcentaje.getText().toString());
        int plazos = 0;

        if(grupo.getCheckedRadioButtonId() == R.id.rdb12){
            plazos = 12;
        }
        else if (grupo.getCheckedRadioButtonId() == R.id.rdb18){
            plazos = 18;
        }
        else if (grupo.getCheckedRadioButtonId() == R.id.rdb24){
            plazos = 24;
        }
        else if (grupo.getCheckedRadioButtonId() == R.id.rdb36){
            plazos = 36;
        }

        Cotizacion cotizacion = new Cotizacion(0, descripcion, valorAuto, porcentajePagoInicial, plazos);
        float pagoInicial = cotizacion.calcularPagoInicial();
        float pagoMensual = cotizacion.calcularPagoMensual();

        txtPagoInicial.setText(String.format("Pago Inicial: %.2f", pagoInicial));
        txtPagoMensual.setText(String.format("Pago Mensual: %.2f", pagoMensual));
    }


    public void iniciarComponentes(){
        txtUsuario = findViewById(R.id.txtUsuario);
        txtFolio = findViewById(R.id.txtFolio);
        txtPagoInicial = findViewById(R.id.txtPagoInicial);
        txtPagoMensual = findViewById(R.id.txtPagoMensual);
        txtPorcentaje = findViewById(R.id.txtPorcentaje);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtDescripcion = findViewById(R.id.txtDescripcion);

        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);
        grupo = findViewById(R.id.grupo);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        coti = new Cotizacion();
    }

}
