package com.example.app93;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
public class MenuActivity extends AppCompatActivity{
    private CardView crv_hola, crv_imc, crv_grados, crv_moneda, crv_cotizacion, crv_spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();

        crv_hola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        crv_imc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ImcActivity.class);
                startActivity(intent);
            }
        });

        crv_grados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, GradosActivity.class);
                startActivity(intent);
            }
        });

        crv_moneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MonedaActivity.class);
                startActivity(intent);
            }
        });

        crv_cotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, IngresaCotizacionActivity.class);
                startActivity(intent);
            }
        });

        crv_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });
    }

    public void iniciarComponentes(){
        crv_hola = (CardView) findViewById(R.id.crv_hola);
        crv_imc = (CardView) findViewById(R.id.crv_imc);
        crv_grados = (CardView) findViewById(R.id.crv_grados);
        crv_moneda = (CardView) findViewById(R.id.crv_moneda);
        crv_cotizacion = (CardView) findViewById(R.id.crv_cotizacion);
        crv_spinner = (CardView) findViewById(R.id.crv_spinner);
    }
}
