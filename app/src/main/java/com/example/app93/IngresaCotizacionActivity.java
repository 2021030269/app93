package com.example.app93;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IngresaCotizacionActivity extends AppCompatActivity{
    private EditText txtUsuario;
    private Button btnIngresar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_ingresa_cotizacion);
        this.init();

        btnIngresar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().equals("")) {
                    Toast.makeText(IngresaCotizacionActivity.this, "Faltó capturar el usuario", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(IngresaCotizacionActivity.this, CotizacionActivity.class);
                intent.putExtra("cliente", txtUsuario.getText().toString());
                startActivity(intent);
            }
        });

        this.btnRegresar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.layout.activity_ingresa_cotizacion) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        this.txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        this.btnIngresar = (Button) findViewById(R.id.btnIngresar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }
}
